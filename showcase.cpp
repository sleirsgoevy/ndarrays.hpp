#include "ndarrays.hpp"
#include <iostream>

using namespace std;
using namespace ndarrays;

template<typename A>
void print_ndarray(const A& arr)
{
    cout << arr.size()[0] << 'x' << arr.size()[1] << ':' << endl;
    for(int i = 0; i < arr.size()[0]; i++)
    {
        for(int j = 0; j < arr.size()[1]; j++)
            cout << arr[idx(i, j)] << ' ';
        cout << endl;
    }
}

template<typename A>
void print_ndarray_1d(const A& arr)
{
    cout << arr.size()[0] << ':';
    for(int i = 0; i < arr.size()[0]; i++)
        cout << ' ' << arr[idx(i)];
    cout << endl;
}

int main(void)
{
    auto arr = ndarray<double>(2, 2);
    arr[idx(0, 0)] = 1;
    arr[idx(0, 1)] = 2;
    arr[idx(1, 0)] = 3;
    arr[idx(1, 1)] = 4;
    print_ndarray(arr);
    print_ndarray(arr + arr);
    print_ndarray(arr - arr);
    print_ndarray(arr * arr);
    print_ndarray(arr / arr);
    print_ndarray(arr * arr - arr);
    print_ndarray_1d((arr * arr - arr).template sum<0>());
    print_ndarray_1d((arr * arr - arr).template prod<0>());
    print_ndarray(arr.template view<2>(idx(2, 2), [&](const Idx<2>& index) { return idx(index[0]^1, index[1]^1); }));
    {
        auto q = (arr + arr).template view<2>(idx(2, 2), [&](const Idx<2>& index) { return idx(index[0]^1, index[1]^1); });
        print_ndarray(q);
    }
    {
        auto deriv = arr.zero_same_size();
        print_ndarray(deriv);
        {
            auto q = arr.with_backprop(deriv).template view<2>(idx(2, 2), [&](const Idx<2>& index) { return idx(index[0]^1, index[1]); });
            auto r = q * q * q * q;
            r.backprop();
        }
        print_ndarray(deriv);
    }
    {
        auto deriv = arr.zero_same_size();
        arr.with_backprop(deriv).template sum<0>().template sum<0>().backprop();
        print_ndarray(deriv);
    }
    {
        auto deriv = arr.zero_same_size();
        {
            auto q = arr.with_backprop(deriv);
            (q * q + q).template sum<0>().template sum<0>().backprop();
        }
        print_ndarray(deriv);
    }
    {
        auto deriv = arr.zero_same_size();
        {
            auto q = arr.with_backprop(deriv);
            (q * q - q).template sum<0>().template sum<0>().backprop();
        }
        print_ndarray(deriv);
    }
    {
        auto deriv = arr.zero_same_size();
        (1 - arr.with_backprop(deriv)).backprop();
        print_ndarray(deriv);
    }
}
