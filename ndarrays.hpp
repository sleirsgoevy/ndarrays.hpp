#include <array>
#include <utility>
#include <cstddef>

namespace ndarrays
{

namespace internal
{

struct False;

struct True
{
    static constexpr bool value = true;
};

struct False
{
    static constexpr bool value = false;
};

template<typename T>
struct HasStealSemantics : False{};

template<typename T, bool is_owning>
struct StolenValue : T
{
    static_assert(HasStealSemantics<T>::value);
    using Number = typename T::Number;
    static constexpr int dimensions = T::dimensions;
    StolenValue(T&& base) : T(std::move(base)){}
    const T* operator->() const
    {
        return this;
    }
    const T& operator[](std::nullptr_t index) const
    {
        return *this;
    }
    const T& operator*() const
    {
        return *this;
    }
    T& mut() const
    {
        return *(StolenValue*)this;
    }
};

template<typename T>
struct StolenValue<T, false>
{
    static_assert(HasStealSemantics<T>::value);
    using Number = typename T::Number;
    static constexpr int dimensions = T::dimensions;
    const T* m_value;
    StolenValue(const T& base) : m_value(&base){}
    const T* operator->() const
    {
        return m_value;
    }
    const T& operator[](std::nullptr_t index) const
    {
        return *m_value;
    }
    const T& operator*() const
    {
        return *m_value;
    }
    T& mut() const
    {
        return *(T*)m_value;
    }
};

//StolenValue should never be accepted by generic overloads
template<typename Base, bool is_owning>
struct HasStealSemantics<StolenValue<Base, is_owning>>{};

template<typename T, bool is_owning, bool has_steal>
struct TryStealImpl
{
    using type = StolenValue<T, is_owning>;
};

template<typename T, bool is_owning>
struct TryStealImpl<T, is_owning, false>{};

template<typename T, bool is_owning>
using TrySteal = typename TryStealImpl<T, is_owning, HasStealSemantics<T>::value>::type;

#define UNBRACE(...) __VA_ARGS__

#define OVERLOAD0(tplt, x, l1, l2, r1, r2, cond, body, ...)\
template<UNBRACE tplt>\
typename std::enable_if<cond, decltype(body((*(UNBRACE l1*)nullptr), (*(UNBRACE r1*)nullptr), __VA_ARGS__))>::type operator x(UNBRACE l1 l2 left, UNBRACE r1 r2 right)\
{\
    return (body((left), (right), __VA_ARGS__));\
}

#define OVERLOAD0_V(tplt, x, l1, l2, r1, r2, cond, val, body, ...)\
template<UNBRACE tplt>\
typename std::enable_if<cond, decltype(body((*(UNBRACE l1*)nullptr), (*(UNBRACE r1*)nullptr), (val((*(UNBRACE l1*)nullptr), (*(UNBRACE r1*)nullptr), __VA_ARGS__)), __VA_ARGS__))>::type operator x(UNBRACE l1 l2 left, UNBRACE r1 r2 right)\
{\
    auto temp_value = val((left), (right), __VA_ARGS__);\
    return (body((left), (right), (temp_value), __VA_ARGS__));\
}

#define OVERLOAD(x, l1, l2, r1, r2, ...) OVERLOAD0((typename Left, typename Right), x, (l1), l2, (r1), r2, __VA_ARGS__)
#define OVERLOAD_V(x, l1, l2, r1, r2, ...) OVERLOAD0_V((typename Left, typename Right), x, (l1), l2, (r1), r2, __VA_ARGS__)

#define OP0(x, a, b, c, d, e, f) OVERLOAD(x, a Left, b, d Right, e, true, BODY0, x, a, b, c, d, e, f)
#define BODY0(left, right, x, a, b, c, d, e, f) TrySteal<Left, c>((a Left b)(left)) x TrySteal<Right, f>((d Right e)(right))

#define OP1(x, a, b, c) OP0(x, a, b, c, const, &, false) OP0(x, a, b, c, , &&, true)\
    OVERLOAD(x, a Left, b, const Right, &, !HasStealSemantics<Right>::value, BODY1, x, a, b, c)\
    OVERLOAD(x, const Left, &, a Right, b, !HasStealSemantics<Left>::value, BODY2, x, a, b, c)
#define BODY1(left, right, x, a, b, c) TrySteal<Left, c>((a Left b)(left)) x right
#define BODY2(left, right, x, a, b, c) left x TrySteal<Right, c>((a Right b)(right))

#define OP(x) OP1(x, const, &, false) OP1(x, , &&, true)

OP(+)
OP(-)
OP(*)
OP(/)

#undef OP
#undef BODY2
#undef BODY1
#undef OP1
#undef BODY0
#undef OP0

template<int dimensions>
using Idx = std::array<size_t, dimensions>;

template<typename N, int d>
struct NdarrayStorage
{
    using Number = N;
    static constexpr int dimensions = d;
    Number* m_data = nullptr;
    Idx<d> m_size = {};
    template<int idx = 0>
    size_t m_raw_size() const
    {
        if constexpr(idx == dimensions - 1)
            return m_size[idx];
        else
            return m_size[idx] * m_raw_size<idx+1>();
    }
    NdarrayStorage(){}
    NdarrayStorage(const NdarrayStorage& other) : NdarrayStorage()
    {
        *this = other;
    }
    NdarrayStorage(NdarrayStorage&& other) : NdarrayStorage()
    {
        *this = std::move(other);
    }
    template<typename Lambda, int i = 0>
    void m_fill_array(Idx<dimensions>& idx, size_t& raw_idx, const Lambda& initializer)
    {
        if constexpr(i == dimensions)
            m_data[raw_idx++] = initializer(idx);
        else
        {
            for(idx[i] = 0; idx[i] < m_size[i]; idx[i]++)
                m_fill_array<Lambda, i+1>(idx, raw_idx, initializer);
        }
    }
    template<typename Lambda>
    NdarrayStorage(const Idx<dimensions>& size, const Lambda& initializer) : m_size(size)
    {
        if(m_raw_size())
            m_data = new Number[m_raw_size()];
        Idx<dimensions> idx;
        size_t raw_idx = 0;
        m_fill_array(idx, raw_idx, initializer);
    }
    NdarrayStorage& operator=(const NdarrayStorage& other)
    {
        if(this != &other)
        {
            if(m_raw_size() != other.m_raw_size())
            {
                delete[] m_data;
                if(other.m_raw_size())
                    m_data = new Number[other.m_raw_size()];
                else
                    m_data = nullptr;
            }
            m_size = other.m_size;
            for(size_t i = 0; i < m_raw_size(); i++)
                m_data[i] = other.m_data[i];
        }
        return *this;
    }
    NdarrayStorage& operator=(NdarrayStorage&& other)
    {
        if(this != &other)
        {
            delete[] m_data;
            m_data = other.m_data;
            m_size = other.m_size;
            other.m_data = nullptr;
            other.m_size = Idx<dimensions>{};
        }
        return *this;
    }
    template<int idx = dimensions - 1>
    size_t m_get_raw_index(const Idx<dimensions>& index) const
    {
        if constexpr(idx == 0)
            return index[0];
        else
            return m_get_raw_index<idx-1>(index) * m_size[idx] + index[idx];
    }
    Number& operator[](const Idx<dimensions>& index)
    {
        return m_data[m_get_raw_index(index)];
    }
    const Number& operator[](const Idx<dimensions>& index) const
    {
        return m_data[m_get_raw_index(index)];
    }
    ~NdarrayStorage()
    {
        if(m_data)
            delete[] m_data;
    }
    const Idx<dimensions>& size() const
    {
        return m_size;
    }
};

template<typename N>
struct NdarrayStorage<N, 0>
{
    using Number = N;
    static constexpr int dimensions = 0;
    Number m_value{};
    Idx<0> m_size;
    NdarrayStorage(){}
    template<typename Lambda>
    NdarrayStorage(const Idx<0>& size, const Lambda& initializer)
    {
        m_value = initializer(size);
    }
    Number& operator[](const Idx<0>& idx)
    {
        return m_value;
    }
    const Number& operator[](const Idx<0>& idx) const
    {
        return m_value;
    }
    const Idx<0>& size() const
    {
        return m_size;
    }
};

template<typename N, int d>
struct NdarrayDup
{
    using Number = N;
    static constexpr int dimensions = d;
    Number m_value{};
    Idx<dimensions> m_size{};
    NdarrayDup(){}
    NdarrayDup(const Idx<dimensions>& size, const Number& value) : m_size(size), m_value(value){}
    Number& operator[](const Idx<dimensions>& idx)
    {
        return m_value;
    }
    const Number& operator[](const Idx<dimensions>& idx) const
    {
        return m_value;
    }
    const Idx<dimensions>& size() const
    {
        return m_size;
    }
};

template<int idst, int isrc, int cnt, typename Dst, typename Src>
static void copy_array(Dst& dst, const Src& src)
{
    if constexpr(cnt)
    {
        dst[idst] = src[isrc];
        copy_array<idst+1, isrc+1, cnt-1>(dst, src);
    }
}

template<typename Base, int d, typename Lambda>
struct NdarrayView
{
    using Number = typename Base::Number;
    static constexpr int dimensions = d;
    Base m_base;
    Idx<dimensions> m_size;
    Lambda m_lambda;
    NdarrayView(Base&& base, const Idx<dimensions>& size, const Lambda& lambda) : m_base(std::move(base)), m_size(size), m_lambda(lambda){}
    Number& operator[](const Idx<dimensions>& idx)
    {
        return m_base.mut()[m_lambda(idx)];
    }
    const Number& operator[](const Idx<dimensions>& idx) const
    {
        return m_base[0][m_lambda(idx)];
    }
    const Idx<dimensions>& size() const
    {
        return m_size;
    }
};

template<typename Base>
struct NdarrayTrait;

template<typename Left, bool left_own, typename Right, bool right_own>
auto make_backprop_wrapper(StolenValue<NdarrayTrait<Left>, left_own>&& left, StolenValue<NdarrayTrait<Right>, right_own>&& right);

template<typename Base>
struct NdarrayTrait : Base
{
    using Number = typename Base::Number;
    static constexpr int dimensions = Base::dimensions;
    NdarrayTrait(Base&& base) : Base(std::move(base)){}
    template<int i, typename Lambda>
    NdarrayTrait<NdarrayStorage<Number, dimensions-1>> reduce(const Lambda& reducer) const
    {
        const NdarrayTrait& self = *this;
        static_assert(i >= 0 && i < dimensions);
        if constexpr(i >= 0 && i < dimensions)
        {
            const Idx<dimensions>& size = Base::size();
            Idx<dimensions-1> new_size;
            copy_array<0, 0, i>(new_size, size);
            copy_array<i, i+1, dimensions-(i+1)>(new_size, size);
            auto ans = NdarrayStorage<Number, dimensions-1>(new_size, [&](const Idx<dimensions-1>& index)
            {
                Idx<dimensions> new_index;
                copy_array<0, 0, i>(new_index, index);
                copy_array<i+1, i, dimensions-(i+1)>(new_index, index);
                new_index[i] = 0;
                Number ans = self[new_index];
                for(new_index[i] = 1; new_index[i] < size[i]; new_index[i]++)
                    ans = reducer(ans, self[new_index]);
                return ans;
            });
            return NdarrayTrait<decltype(ans)>(std::move(ans));
        }
    }
    template<int i>
    NdarrayTrait<NdarrayStorage<Number, dimensions-1>> sum() const
    {
        return reduce<i>([](const Number& a, const Number& b)
        {
            return a + b;
        });
    }
    template<int i>
    NdarrayTrait<NdarrayStorage<Number, dimensions-1>> prod() const
    {
        return reduce<i>([](const Number& a, const Number& b)
        {
            return a * b;
        });
    }
    NdarrayTrait<NdarrayStorage<Number, dimensions>> operator-() const
    {
        auto& self = *this;
        return NdarrayTrait<NdarrayStorage<Number, dimensions>>(NdarrayStorage<Number, dimensions>(Base::size(), [&](const Idx<dimensions>& idx)
        {
            return -self[idx];
        }));
    }
    NdarrayTrait<NdarrayStorage<Number, dimensions>> inverse() const
    {
        auto& self = *this;
        return NdarrayTrait<NdarrayStorage<Number, dimensions>>(NdarrayStorage<Number, dimensions>(Base::size(), [&](const Idx<dimensions>& idx)
        {
            return 1 / self[idx];
        }));
    }
    template<typename Lambda>
    auto map(const Lambda& l) const
    {
        auto& self = *this;
        using Y = decltype(l(*(Number*)nullptr));
        return NdarrayTrait<NdarrayStorage<Y, dimensions>>(NdarrayStorage<Y, dimensions>(Base::size(), [&](const Idx<dimensions>& idx)
        {
            return l(self[idx]);
        }));
    }
    template<int dimensions, typename Lambda>
    auto view(const Idx<dimensions>& new_size, const Lambda& lambda) const&
    {
        return NdarrayTrait<NdarrayView<StolenValue<NdarrayTrait, false>, dimensions, Lambda>>(NdarrayView<StolenValue<NdarrayTrait, false>, dimensions, Lambda>(StolenValue<NdarrayTrait, false>(*this), new_size, lambda));
    }
    template<int dimensions, typename Lambda>
    auto view(const Idx<dimensions>& new_size, const Lambda& lambda)&&
    {
        return NdarrayTrait<NdarrayView<StolenValue<NdarrayTrait, true>, dimensions, Lambda>>(NdarrayView<StolenValue<NdarrayTrait, true>, dimensions, Lambda>(StolenValue<NdarrayTrait, true>(std::move(*this)), new_size, lambda));
    }
#define AXIS_OPS(A, B, C)\
    template<int axis>\
    auto add_axis(size_t size) A\
    {\
        static_assert(axis >= 0 && axis <= dimensions);\
        Idx<dimensions+1> new_size;\
        const auto& old_size = Base::size();\
        copy_array<0, 0, axis>(new_size, old_size);\
        copy_array<axis+1, axis, dimensions-axis>(new_size, old_size);\
        new_size[axis] = size;\
        auto lambda = [&](const Idx<dimensions+1>& idx)\
        {\
            Idx<dimensions> outer_idx;\
            copy_array<0, 0, axis>(outer_idx, idx);\
            copy_array<axis, axis+1, dimensions-axis>(outer_idx, idx);\
            return outer_idx;\
        };\
        return NdarrayTrait<NdarrayView<StolenValue<NdarrayTrait, B>, dimensions+1, decltype(lambda)>>(NdarrayView<StolenValue<NdarrayTrait, B>, dimensions+1, decltype(lambda)>(StolenValue<NdarrayTrait, B>(C(*this)), new_size, lambda));\
    }\
    template<int axis>\
    auto index(size_t index) A\
    {\
        static_assert(axis >= 0 && axis < dimensions);\
        Idx<dimensions-1> new_size;\
        const auto& old_size = Base::size();\
        copy_array<0, 0, axis>(new_size, old_size);\
        copy_array<axis, axis+1, dimensions-axis-1>(new_size, old_size);\
        return C(*this).template view<dimensions-1>(new_size, [index](const Idx<dimensions-1>& idx)\
        {\
            Idx<dimensions> outer_idx;\
            copy_array<0, 0, axis>(outer_idx, idx);\
            copy_array<axis+1, axis, dimensions-axis-1>(outer_idx, idx);\
            outer_idx[axis] = index;\
            return outer_idx;\
        });\
    }\
    template<int a1, int a2>\
    auto swap_axes() A\
    {\
        static_assert(a1 >= 0 && a1 < dimensions && a2 >= 0 && a2 < dimensions && a1 != a2);\
        auto new_size = Base::size();\
        std::swap(new_size[a1], new_size[a2]);\
        return C(*this).template view<dimensions>(new_size, [](const Idx<dimensions>& idx)\
        {\
            Idx<dimensions> new_idx = idx;\
            std::swap(new_idx[a1], new_idx[a2]);\
            return new_idx;\
        });\
    }
    AXIS_OPS(const&, false, )
    AXIS_OPS(&&, true, std::move)
#undef AXIS_OPS
    auto zero_same_size() const
    {
        return NdarrayTrait<NdarrayStorage<Number, dimensions>>(NdarrayStorage<Number, dimensions>(Base::size(), [&](const Idx<dimensions>& idx)
        {
            return Number{};
        }));
    }
    auto with_backprop() const&
    {
        return with_backprop(zero_same_size());
    }
    auto with_backprop()&&
    {
        auto zss = zero_same_size();
        return std::move(*this).with_backprop(std::move(zss));
    }
    template<typename Other>
    auto with_backprop(Other& other) const&
    {
        return make_backprop_wrapper(StolenValue<NdarrayTrait, false>(*this), StolenValue<Other, false>(other));
    }
    template<typename Other>
    auto with_backprop(Other&& other) const&
    {
        return make_backprop_wrapper(StolenValue<NdarrayTrait, false>(*this), StolenValue<Other, true>(std::move(other)));
    }
    template<typename Other>
    auto with_backprop(Other& other)&&
    {
        return make_backprop_wrapper(StolenValue<NdarrayTrait, true>(std::move(*this)), StolenValue<Other, false>(other));
    }
    template<typename Other>
    auto with_backprop(Other&& other)&&
    {
        return make_backprop_wrapper(StolenValue<NdarrayTrait, true>(std::move(*this)), StolenValue<Other, true>(std::move(other)));
    }
};

template<typename N, int d>
struct BackpropBase : private NdarrayTrait<NdarrayStorage<N, d>>
{
    using Number = N;
    static constexpr int dimensions = d;
    NdarrayTrait<NdarrayStorage<Number, dimensions>> m_deriv;
    template<typename Lambda>
    BackpropBase(const Idx<dimensions>& size, const Lambda& initializer) :
        NdarrayTrait<NdarrayStorage<Number, dimensions>>(NdarrayStorage<Number, dimensions>(size, initializer)),
        m_deriv(NdarrayStorage<Number, dimensions>(size, [](const Idx<dimensions>& index)
        {
            return Number{};
        })){}
    BackpropBase() : BackpropBase(Idx<dimensions>{}, [&](const Idx<dimensions>& index)
    {
        return Number{};
    }){}
    NdarrayTrait<NdarrayStorage<Number, dimensions>>& values()
    {
        return *this;
    }
    const NdarrayTrait<NdarrayStorage<Number, dimensions>>& values() const
    {
        return *this;
    }
    NdarrayTrait<NdarrayStorage<Number, dimensions>>& derivs() const
    {
        return ((BackpropBase*)this)->m_deriv;
    }
    using NdarrayTrait<NdarrayStorage<Number, dimensions>>::size;
    using NdarrayTrait<NdarrayStorage<Number, dimensions>>::operator[];
    template<int i = 0, typename Lambda>
    void m_for_each_index(Idx<dimensions>& idx, const Idx<dimensions>& size, const Lambda& functor) const
    {
        if constexpr(i == dimensions)
            functor(idx);
        else
        {
            for(idx[i] = 0; idx[i] < size[i]; idx[i]++)
                m_for_each_index<i+1>(idx, size, functor);
        }
    }
    template<typename Lambda>
    void m_for_each_index(const Lambda& functor)
    {
        Idx<dimensions> index;
        m_for_each_index<0>(index, NdarrayTrait<NdarrayStorage<Number, dimensions>>::size(), functor);
    }
};

template<typename Left, typename Right>
struct BackpropWrapper
{
    static_assert(std::is_same_v<typename Left::Number, typename Right::Number> && Left::dimensions == Right::dimensions);
    using Number = typename Left::Number;
    static constexpr int dimensions = Left::dimensions;
    Left m_left;
    Right m_right;
    BackpropWrapper(Left&& left, Right&& right) : m_left(std::move(left)), m_right(std::move(right)){}
    const auto& values() const
    {
        return *m_left;
    }
    auto& derivs() const
    {
        return m_right.mut();
    }
    const Idx<dimensions>& size() const
    {
        return m_left->size();
    }
    const auto& operator[](const Idx<dimensions>& idx) const
    {
        return m_left[0][idx];
    }
};

template<typename Base, int d, typename Lambda>
struct BackpropView : Base
{
    using Number = typename Base::Number;
    static constexpr int dimensions = d;
    decltype((*(Base*)nullptr)->values().template view<dimensions>(*(Idx<dimensions>*)nullptr, *(Lambda*)nullptr)) m_values;
    decltype((*(Base*)nullptr)->derivs().template view<dimensions>(*(Idx<dimensions>*)nullptr, *(Lambda*)nullptr)) m_derivs;
    BackpropView(Base&& base, const Idx<dimensions>& size, const Lambda& lambda) : Base(std::move(base)),
        m_values(((Base*)this)[0]->values().template view<dimensions>(size, lambda)),
        m_derivs(((Base*)this)[0]->derivs().template view<dimensions>(size, lambda)){}
    auto& values()
    {
        return m_values;
    }
    const auto& values() const
    {
        return m_values;
    }
    auto& derivs() const
    {
        return ((BackpropView*)this)->m_derivs;
    }
    const Idx<dimensions>& size() const
    {
        return m_values.size();
    }
    Number& operator[](const Idx<dimensions>& idx)
    {
        return m_values[idx];
    }
    const Number& operator[](const Idx<dimensions>& idx) const
    {
        return m_values[idx];
    }
};

template<bool is_const, typename Left, typename Right, typename N = decltype(*(typename Left::Number*)nullptr + *(typename Right::Number*)nullptr)>
struct Add : BackpropBase<N, Left::dimensions>
{
    static_assert(Left::dimensions == Right::dimensions);
    using Number = N;
    static constexpr int dimensions = Left::dimensions;
    Left m_left;
    Right m_right;
    Add(Left&& left, Right&& right) : m_left(std::move(left)), m_right(std::move(right))
    {
        *(BackpropBase<Number, dimensions>*)this = BackpropBase<Number, dimensions>(left->size(), [&](const Idx<dimensions>& idx)
        {
            return left[0][idx] + right[0][idx];
        });
    }
    ~Add()
    {
        BackpropBase<Number, dimensions>::m_for_each_index([&](const Idx<dimensions>& idx)
        {
            m_left->derivs()[idx] += BackpropBase<Number, dimensions>::derivs()[idx];
            m_right->derivs()[idx] += BackpropBase<Number, dimensions>::derivs()[idx];
        });
    }
};

template<typename Left, typename Right, typename N>
struct Add<true, Left, Right, N> : private NdarrayTrait<NdarrayStorage<N, Left::dimensions>>
{
    static_assert(Left::dimensions == Right::dimensions);
    using Number = N;
    static constexpr int dimensions = Left::dimensions;
    using NdarrayTrait<NdarrayStorage<Number, dimensions>>::size;
    using NdarrayTrait<NdarrayStorage<Number, dimensions>>::operator[];
    Left m_left;
    Right m_right;
    Add(Left&& left, Right&& right) : m_left(std::move(left)), m_right(std::move(right)),
        NdarrayTrait<NdarrayStorage<N, Left::dimensions>>(NdarrayStorage<N, Left::dimensions>(Idx<dimensions>(), [&](const Idx<dimensions>& idx)
        {
            return Number{};
        }))
    {
        *(NdarrayTrait<NdarrayStorage<Number, dimensions>>*)this = NdarrayTrait<NdarrayStorage<Number, dimensions>>(NdarrayStorage<Number, dimensions>(m_left->size(), [&](const Idx<dimensions>& idx)
        {
            return left[0][idx] + right[0][idx];
        }));
    }
    NdarrayTrait<NdarrayStorage<Number, dimensions>>& values()
    {
        return *this;
    }
    const NdarrayTrait<NdarrayStorage<Number, dimensions>>& values() const
    {
        return *this;
    }
    auto& derivs() const
    {
        return m_left->derivs();
    }
};

template<bool is_const, typename Left, typename Right, typename N = decltype(*(typename Left::Number*)nullptr * *(typename Right::Number*)nullptr)>
struct Mul : BackpropBase<N, Left::dimensions>
{
    static_assert(Left::dimensions == Right::dimensions);
    static_assert(sizeof((*(Left*)nullptr)->derivs()) >= 0);
    using Number = N;
    static constexpr int dimensions = Left::dimensions;
    using BackpropBase<N, Left::dimensions>::derivs;
    Left m_left;
    Right m_right;
    Mul(Left&& left, Right&& right) : m_left(std::move(left)), m_right(std::move(right))
    {
        *(BackpropBase<Number, dimensions>*)this = BackpropBase<Number, dimensions>(m_left->size(), [&](const Idx<dimensions>& idx)
        {
            return m_left[0][idx] * m_right[0][idx];
        });
    }
    ~Mul()
    {
        BackpropBase<Number, dimensions>::m_for_each_index([&](const Idx<dimensions>& idx)
        {
            m_left->derivs()[idx] += derivs()[idx] * m_right[0][idx];
            if constexpr(!is_const)
                m_right->derivs()[idx] += derivs()[idx] * m_left[0][idx];
        });
    }
};

template<int axis, typename Left>
struct AxisAdd : BackpropBase<typename Left::Number, Left::dimensions - 1>
{
    using Number = typename Left::Number;
    static constexpr int dimensions = Left::dimensions - 1;
    Left m_left;
    AxisAdd(Left&& left) : m_left(std::move(left))
    {
        const auto& old_size = m_left->size();
        Idx<dimensions> size;
        copy_array<0, 0, axis>(size, old_size);
        copy_array<axis, axis+1, dimensions-axis>(size, old_size);
        *(BackpropBase<Number, dimensions>*)this = BackpropBase<Number, dimensions>(size, [&](const Idx<dimensions>& idx)
        {
            Idx<dimensions+1> parent_idx;
            copy_array<0, 0, axis>(parent_idx, idx);
            copy_array<axis+1, axis, dimensions-axis>(parent_idx, idx);
            parent_idx[axis] = 0;
            Number ans = m_left[0][parent_idx];
            for(parent_idx[axis] = 1; parent_idx[axis] < old_size[axis]; parent_idx[axis]++)
                ans += m_left[0][parent_idx];
            return ans;
        });
    }
    ~AxisAdd()
    {
        const auto& old_size = m_left->size();
        BackpropBase<Number, dimensions>::m_for_each_index([&](const Idx<dimensions>& idx)
        {
            Idx<dimensions+1> parent_idx;
            copy_array<0, 0, axis>(parent_idx, idx);
            copy_array<axis+1, axis, dimensions-axis>(parent_idx, idx);
            for(parent_idx[axis] = 0; parent_idx[axis] < old_size[axis]; parent_idx[axis]++)
                m_left->derivs()[parent_idx] += BackpropBase<Number, dimensions>::derivs()[idx];
        });
    }
};

template<typename Base>
struct HasStealSemantics<NdarrayTrait<Base>> : True{};

template<typename Left, typename Lambda1, typename Lambda2, typename N = decltype((*(Lambda1*)nullptr)(*(typename Left::Number*)nullptr))>
struct FunctorApplication : BackpropBase<N, Left::dimensions>
{
    using Number = N;
    static constexpr int dimensions = Left::dimensions;
    using BackpropBase<N, Left::dimensions>::values;
    using BackpropBase<N, Left::dimensions>::derivs;
    Left m_left;
    Lambda1 m_func;
    Lambda2 m_deriv;
    FunctorApplication(Left&& left, const Lambda1& l1, const Lambda2& l2) : m_left(std::move(left)), m_func(l1), m_deriv(l2)
    {
        *(BackpropBase<Number, dimensions>*)this = BackpropBase<Number, dimensions>(left->size(), [&](const Idx<dimensions>& idx)
        {
            return m_func(m_left[0][idx]);
        });
    }
    ~FunctorApplication()
    {
        BackpropBase<Number, dimensions>::m_for_each_index([&](const Idx<dimensions>& idx)
        {
            m_left->derivs()[idx] += derivs()[idx] * m_deriv(m_left[0][idx], values()[idx]);
        });
    }
};

template<typename T>
auto negate(const T& arg)
{
    return -arg;
}

template<typename T, typename U>
auto negate_deriv(const T& arg, const U& value)
{
    return -1;
}

template<typename T>
auto inverse_fn(const T& arg)
{
    return 1 / arg;
}

template<typename T, typename U>
auto inverse_deriv(const T& arg, const U& value)
{
    return -value*value;
}

template<typename Base>
struct BackpropTrait : Base
{
    using Number = typename Base::Number;
    static constexpr int dimensions = Base::dimensions;
    BackpropTrait(Base&& value) : Base(std::move(value)){}
    auto backprop() const
    {
        //should only be called on scalars
        Base::derivs()[Idx<dimensions>{}] += 1;
    }
    template<int idx>
    auto sum() const&
    {
        static_assert(idx >= 0 && idx < dimensions);
        auto ans = AxisAdd<idx, StolenValue<BackpropTrait<Base>, false>>(StolenValue<BackpropTrait<Base>, false>(*this));
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    template<int idx>
    auto sum()&&
    {
        static_assert(idx >= 0 && idx < dimensions);
        auto ans = AxisAdd<idx, StolenValue<BackpropTrait<Base>, true>>(StolenValue<BackpropTrait<Base>, true>(std::move(*this)));
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    template<int idx>
    void prod() const =delete;
    auto operator-() const&
    {
        using Y = decltype(negate(*(Number*)nullptr));
        auto ans = FunctorApplication(StolenValue<BackpropTrait<Base>, false>(*this), &negate<Number>, &negate_deriv<Number, Y>);
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    auto operator-()&&
    {
        using Y = decltype(negate(*(Number*)nullptr));
        auto ans = FunctorApplication(StolenValue<BackpropTrait<Base>, true>(std::move(*this)), &negate<Number>, &negate_deriv<Number, Y>);
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    auto inverse() const&
    {
        using Y = decltype(inverse_fn(*(Number*)nullptr));
        auto ans = FunctorApplication(StolenValue<BackpropTrait<Base>, false>(*this), &inverse_fn<Number>, &inverse_deriv<Number, Y>);
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    auto inverse()&&
    {
        using Y = decltype(inverse_fn(*(Number*)nullptr));
        auto ans = FunctorApplication(StolenValue<BackpropTrait<Base>, true>(std::move(*this)), &inverse_fn<Number>, &inverse_deriv<Number, Y>);
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    template<typename Lambda1, typename Lambda2>
    auto map(const Lambda1& l1, const Lambda2& l2) const&
    {
        auto ans = FunctorApplication(StolenValue<BackpropTrait<Base>, false>(*this), l1, l2);
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    template<typename Lambda1, typename Lambda2>
    auto map(const Lambda1& l1, const Lambda2& l2)&&
    {
        auto ans = FunctorApplication(StolenValue<BackpropTrait<Base>, true>(std::move(*this)), l1, l2);
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    template<int dimensions, typename Lambda>
    auto view(const Idx<dimensions>& new_size, const Lambda& l) const&
    {
        auto ans = BackpropView<StolenValue<BackpropTrait, false>, dimensions, Lambda>(StolenValue<BackpropTrait, false>(*this), new_size, l);
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
    template<int dimensions, typename Lambda>
    auto view(const Idx<dimensions>& new_size, const Lambda& l)&&
    {
        auto ans = BackpropView<StolenValue<BackpropTrait, true>, dimensions, Lambda>(StolenValue<BackpropTrait, true>(std::move(*this)), new_size, l);
        return BackpropTrait<decltype(ans)>(std::move(ans));
    }
#define AXIS_OPS(A, B)\
    template<int axis>\
    auto add_axis(size_t size) A\
    {\
        static_assert(axis >= 0 && axis <= dimensions);\
        Idx<dimensions+1> new_size;\
        const auto& old_size = Base::size();\
        copy_array<0, 0, axis>(new_size, old_size);\
        copy_array<axis+1, axis, dimensions-axis>(new_size, old_size);\
        new_size[axis] = size;\
        return B(*this).template view<dimensions+1>(new_size, [](const Idx<dimensions+1>& idx)\
        {\
            Idx<dimensions> outer_idx;\
            copy_array<0, 0, axis>(outer_idx, idx);\
            copy_array<axis, axis+1, dimensions-axis>(outer_idx, idx);\
            return outer_idx;\
        });\
    }\
    template<int axis>\
    auto index(size_t index) A\
    {\
        static_assert(axis >= 0 && axis < dimensions);\
        Idx<dimensions-1> new_size;\
        const auto& old_size = Base::size();\
        copy_array<0, 0, axis>(new_size, old_size);\
        copy_array<axis, axis+1, dimensions-axis-1>(new_size, old_size);\
        return B(*this).template view<dimensions-1>(new_size, [index](const Idx<dimensions+1>& idx)\
        {\
            Idx<dimensions> outer_idx;\
            copy_array<0, 0, axis>(outer_idx, idx);\
            copy_array<axis+1, axis, dimensions-axis-1>(outer_idx);\
            outer_idx[axis] = index;\
            return outer_idx;\
        });\
    }\
    template<int a1, int a2>\
    auto swap_axes() A\
    {\
        static_assert(a1 >= 0 && a1 < dimensions && a2 >= 0 && a2 < dimensions && a1 != a2);\
        auto new_size = Base::size();\
        std::swap(new_size[a1], new_size[a2]);\
        return B(*this).template view<dimensions>(new_size, [](const Idx<dimensions>& idx)\
        {\
            Idx<dimensions> outer_idx = idx;\
            std::swap(outer_idx[a1], outer_idx[a2]);\
            return outer_idx;\
        });\
    }
    AXIS_OPS(const&, )
    AXIS_OPS(&&, std::move)
#undef ADD_AXIS
};

template<typename Base>
struct HasStealSemantics<BackpropTrait<Base>> : True{};

template<typename Left, bool left_own, typename Right, bool right_own>
auto make_backprop_wrapper(StolenValue<NdarrayTrait<Left>, left_own>&& left, StolenValue<NdarrayTrait<Right>, right_own>&& right)
{
    auto ans = BackpropWrapper(std::move(left), std::move(right));
    return BackpropTrait(std::move(ans));
}

template<typename T>
struct IsPrimitiveType : True{};

template<typename T, bool is_owning>
struct IsPrimitiveType<StolenValue<T, is_owning>> : False{};

template<typename T>
struct IsPrimitiveType<NdarrayTrait<T>> : False{};

template<typename T>
struct IsPrimitiveType<BackpropTrait<T>> : False{};

#define TPLT (typename Left, bool left_own, typename Right, bool right_own)
#define OP1(op, clazz, a, b, q, lr) OVERLOAD0(TPLT, op, (StolenValue<a<Left>, left_own>), &&, (StolenValue<b<Right>, right_own>), &&, true, BODY1, clazz, a, b, q, lr)
#define BODY1(left, right, clazz, a, b, q, lr) BackpropTrait(clazz<q, lr((StolenValue<a<Left>, left_own>), (StolenValue<b<Right>, right_own>))>(lr((std::move(left)), (std::move(right)))))
#define LR(left, right) UNBRACE left, UNBRACE right
#define RL(left, right) UNBRACE right, UNBRACE left

#define OP(op, clazz)\
    OP1(op, clazz, BackpropTrait, BackpropTrait, false, LR)\
    OP1(op, clazz, BackpropTrait, NdarrayTrait, true, LR)\
    OP1(op, clazz, NdarrayTrait, BackpropTrait, true, RL)\
    OVERLOAD0_V((typename Left, bool left_own, typename Right), op, (StolenValue<BackpropTrait<Left>, left_own>), &&, (const Right), &, IsPrimitiveType<Right>::value, LEFT_SIZE, BODY2, clazz)\
    OVERLOAD0_V((typename Left, typename Right, bool right_own), op, (const Left), &, (StolenValue<BackpropTrait<Right>, right_own>), &&, IsPrimitiveType<Left>::value, RIGHT_SIZE, BODY3, clazz)
#define LEFT_SIZE(left, right, clazz) left->size()
#define BODY2(left, right, sz, clazz) BackpropTrait(clazz<true, StolenValue<BackpropTrait<Left>, left_own>, StolenValue<NdarrayTrait<NdarrayDup<Right, Left::dimensions>>, true>>(std::move(left), StolenValue<NdarrayTrait<NdarrayDup<Right, Left::dimensions>>, true>(NdarrayTrait(NdarrayDup<Right, Left::dimensions>(sz, right)))))
#define RIGHT_SIZE(left, right, clazz) right->size()
#define BODY3(left, right, sz, clazz) BackpropTrait(clazz<true, StolenValue<BackpropTrait<Right>, right_own>, StolenValue<NdarrayTrait<NdarrayDup<Left, Right::dimensions>>, true>>(std::move(right), StolenValue<NdarrayTrait<NdarrayDup<Left, Right::dimensions>>, true>(NdarrayTrait(NdarrayDup<Left, Right::dimensions>(sz, left)))))

OP(+, Add)
OP(*, Mul)

#undef BODY3
#undef RIGHT_SIZE
#undef BODY2
#undef LEFT_SIZE
#undef OP
#undef BODY1
#undef OP1
#undef TPLT

template<typename T>
auto negate_stolen(StolenValue<T, false>&& value)
{
    auto ans = -(*value);
    return StolenValue<decltype(ans), true>(std::move(ans));
}

template<typename T>
auto negate_stolen(StolenValue<T, true>&& value)
{
    auto ans = -std::move(value.mut());
    return StolenValue<decltype(ans), true>(std::move(ans));
}

template<typename T>
auto invert_stolen(StolenValue<T, false>&& value)
{
    auto ans = value->inverse();
    return StolenValue<decltype(ans), false>(std::move(ans));
}

template<typename T>
auto invert_stolen(StolenValue<T, true>&& value)
{
    auto ans = std::move(value.mut()).invert();
    return StolenValue<decltype(ans), false>(std::move(ans));
}

//cannot use OVERLOAD here, lambda expressions in unevaluated context

#define OP(x) \
template<typename Left, typename Right>\
NdarrayTrait<NdarrayStorage<decltype((*(typename Left::Number*)nullptr) x (*(typename Right::Number*)nullptr)), Left::dimensions>> operator x(const NdarrayTrait<Left>& left, const NdarrayTrait<Right>& right)\
{\
    static_assert(Left::dimensions == Right::dimensions);\
    auto ans = NdarrayStorage<decltype((*(typename Left::Number*)nullptr) x (*(typename Right::Number*)nullptr)), Left::dimensions>(left.size(), [&](const Idx<Left::dimensions>& idx)\
    {\
        return left[idx] x right[idx];\
    });\
    return NdarrayTrait(std::move(ans));\
}\
template<typename Left, typename Right>\
typename std::enable_if<IsPrimitiveType<Right>::value, NdarrayTrait<NdarrayStorage<decltype((*(typename Left::Number*)nullptr) x (*(Right*)nullptr)), Left::dimensions>>>::type operator x(const NdarrayTrait<Left>& left, const Right& right)\
{\
    auto ans = NdarrayStorage<decltype((*(typename Left::Number*)nullptr) x right), Left::dimensions>(left.size(), [&](const Idx<Left::dimensions>& idx)\
    {\
        return left[idx] x right;\
    });\
    return NdarrayTrait(std::move(ans));\
}\
template<typename Left, typename Right>\
typename std::enable_if<IsPrimitiveType<Left>::value, NdarrayTrait<NdarrayStorage<decltype((*(Left*)nullptr) x (*(typename Right::Number*)nullptr)), Right::dimensions>>>::type operator x(const Left& left, const NdarrayTrait<Right>& right)\
{\
    auto ans = NdarrayStorage<decltype(left x *(typename Right::Number*)nullptr), Right::dimensions>(right.size(), [&](const Idx<Right::dimensions>& idx)\
    {\
        return left x right[idx];\
    });\
    return NdarrayTrait(std::move(ans));\
}

OP(+)
OP(-)
OP(*)
OP(/)

#undef OP

template<typename T>
struct IsBackprop : False{};

template<typename T>
struct IsBackprop<BackpropTrait<T>> : True{};

#define OP(x, y, q, fn)\
    OVERLOAD0((typename Left, bool left_own, typename Right, bool right_own), x, (StolenValue<Left, left_own>), &&, (StolenValue<Right, right_own>), &&, IsBackprop<Left>::value || IsBackprop<Right>::value, BODY1, y, fn)\
    OVERLOAD0((typename Left, bool left_own, typename Right), x, (StolenValue<Left, left_own>), &&, (const Right), &, IsBackprop<Left>::value && IsPrimitiveType<Right>::value, BODY2, x, y, q, fn)\
    OVERLOAD0((typename Left, typename Right, bool right_own), x, (const Left), &, (StolenValue<Right, right_own>), &&, IsPrimitiveType<Left>::value && IsBackprop<Right>::value, BODY3, y, fn)
#define BODY1(left, right, y, fn) std::move(left) y fn(std::move(right))
#define BODY2(left, right, x, y, q, fn) left y (q x std::move(right))
#define BODY3(left, right, y, fn) left y fn(std::move(right))

OP(-, +, , negate_stolen)
OP(/, *, 1, invert_stolen)

#undef BODY2
#undef BODY1
#undef OP

// we no longer need the OVERLOAD macro, get rid of it
#undef OVERLOAD_V
#undef OVERLOAD
#undef OVERLOAD0_V
#undef OVERLOAD0
#undef UNBRACE

template<typename Dummy, typename... Args>
static constexpr int n_arguments()
{
    return n_arguments<Args...>() + 1;
}

template<>
constexpr int n_arguments<int>()
{
    return 0;
}

}

using internal::Idx;

template<typename... Args>
auto idx(const Args&... size)
{
    return Idx<internal::n_arguments<Args..., int>()>({(size_t)size...});
}

template<typename Lambda, typename... Args>
auto array(const Lambda& l, const Args&... size)
{
    constexpr int d = internal::n_arguments<Args..., int>();
    Idx<d> sz({(size_t)size...});
    return internal::NdarrayTrait(internal::NdarrayStorage<decltype(l(sz)), d>(sz, l));
}

template<typename Number, typename... Args>
auto ndarray(const Args&... size)
{
    constexpr int d = internal::n_arguments<Args..., int>();
    Idx<d> sz({(size_t)size...});
    return internal::NdarrayTrait(internal::NdarrayStorage<Number, d>(sz, [&](const auto& idx)
    {
        return Number{};
    }));
}

}
