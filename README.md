# ndarrays.hpp

This is a minimalistic C++ header-only library implementing a container similar in semantics to Numpy's ndarray or PyTorch's tensor. It supports vectorized arithmetic operations (though I haven't verified that they actually get compiled into vector instructions), sum/product along an axis, axis swaps and function applications.

## General notes

Types returned by any ndarray.hpp APIs should be considered Voldemort types, use "auto" if you need to assign the values to variables. It is not guaranteed that any of the returned values, including from identical function invocations, will have the same type.

See [showcase.cpp](https://gitlab.com/sleirsgoevy/ndarrays.hpp/-/blob/master/showcase.cpp) for a crash course on how to use the library; unlike the library code itself, it should be self-explanatory.

## Example

```cpp
using namespace std;
using namespace ndarrays;

    auto array = ndarray<double>(2, 2); //2-dimentional array of doubles with size 2x2
    // fill it with values
    for(int i = 0; i < 2; i++)
        for(int j = 0; j < 2; j++)
            array[idx(i, j)] = i+2*j+1; //indexing single array elements
    auto deriv = array.zero_same_size(); //shortcut to create an array of the same size
    {
        auto x = arr.with_backprop(deriv); //creates an array with an attached derivative store
        auto y = q * q - q; //calculate some expression with the provided array
        auto loss = y.template sum<0>().template sum<0>(); //sum along both axes to get a scalar
        loss.backprop(); //this configures the loss as the source of backward propagation
    } //the actual backward propagation happens here (in value destructors)
    //now print the calculated gradient of the loss
    for(int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 2; j++)
            cout << deriv[idx(i, j)] << ' ';
        cout << endl;
    }
```

## Lifetimes

**Note**: the following applies mainly to backward propagation. Ndarrays without backward propagation usually do not keep references to each other.

If a value involved in an operation (arithmetic operator or method call) is passed by rvalue reference, the value is moved into the result, and its lifetime is extended to that of the result. If a value is passed by a normal reference, it is kept in place and its lifetime is not extended; it's the responsibility of the caller to ensure the correct deinitialization order.

## API reference

**Note**: in the following text, `ndarray<T, N>` refers to an N-dimentional ndarray with base type T, and `backprop<T, N>` refers to an N-dimentional array with back propagation (also with base type T). The actual types are named in an undocumented way, and it's not guaranteed that any two `ndarray<T, N>` or `backprop<T, N>` are of the same type (usually they are not). `ndarrays::Idx<N>` is a convenience alias to `std::array<size_t, N>`.

Arithmetic operators (+, -, /, \*) are overloaded for `ndarray<T, N>`, `backprop<T, N>`, a combination of them, or a combination of one of those with a scalar.

Additionally, the following instance methods are provided:

```cpp
/* ndarray methods */
ndarrays::Idx<N> ndarray<T, N>::size();
T ndarray<T, N>::operator[](ndarrays::Idx<N>);
ndarray<T, N-1> ndarray<T, N>::template reduce<i>(const Lambda& reducer); //treats the lambda as a binary operation and applies it left-to-right along the specified axis
ndarray<T, N-1> ndarray<T, N>::template sum<i>(); //sums along the specified axis
ndarray<T, N-1> ndarray<T, N>::template prod<i>(); //multiplies along the specified axis
ndarray<T, N> ndarray<T, N>::inverse(); //applies the 1/x function to every element
ndarray<T, N> ndarray<T, N>::map(const Lambda& f); //applies the given function to every element
ndarray<T, M> ndarray<T, N>::view(ndarrays::Idx<M> new_size, const Lambda& lambda); //creates a permutation of the original array. the lambda takes in the coordinates in the new array and returns ones from the old array. this is not a copy, changing values within the view modifies the original array
ndarray<T, N+1> ndarray<T, N>::template add_axis<i>(size_t size); //inserts an axis into the array. cells in the new array reference the same cells in the old array, regardless of the index along the new axis. this is not a copy, changing values within the view modifies the original array
ndarray<T, N-1> ndarray<T, N>::template index<i>(size_t idx); //partially indexes the array by an arbitrary axis. the resulting array has the elements as if the index along this axis was idx. this is not a copy, changing values within the view modifies the original array
ndarray<T, N> ndarray<T, N>::template swap_axes<i, j>(); //swaps axes i, j in the array. this is not a copy, changing values within the view modifies the original array
ndarray<T, N> ndarray<T, N>::zero_same_size(); //returns an array of the same size filled with zeroes. this is most useful for explicitly creating a backing store for the gradient
backprop<T, N> ndarray<T, N>::with_backprop(); //attaches a zero-initialized backing store for the gradient to the array. this array can be referenced with the derivs() method
backprop<T, N> ndarray<T, N>::with_backprop(ndarray<T, N>& other); //attaches an explicitly specified backing store for the gradients to the array. this array should be exactly the same size

/* backprop methods */
ndarray<T, N>& backprop<T, N>::values(); //returns the raw array used to store values
ndarray<T, N>& backprop<T, N>::derivs(); //returns the raw array used to store the gradient. note: this returns a non-constant reference even if called on a constant object
ndarrays::Idx<N> backprop<T, N>::size();
T backprop<T, N>::operator[](ndarrays::Idx<N> idx); //same as this->values()[idx]
void backprop<T, N>::backprop(); //sets the specified scalar as the backward propagation source
backprop<T, N> backprop<T, N>::template sum<i>(); //sum along an axis
backprop<T, N> backprop<T, N>::inverse(); //applies 1/x to every element
backprop<T, N> backprop<T, N>::map(const Lambda1& lambda1, const Lambda2& lambda2); //applies the given function to every element. the first lambda is f(x), the second lambda is df_dx(x, f(x))

/* backprop methods, identical to the corresponding ndarray methods */
backprop<T, M> backprop<T, N>::view(ndarrays::Idx<M> new_size, const Lambda& lambda);
backprop<T, N+1> backprop<T, N>::template add_axis<i>(size_t size);
backprop<T, N-1> backprop<T, N>::template index<i>(size_t idx);
backprop<T, N> backprop<T, N>::template swap_axes<i, j>();
```
